﻿using LowRezJam.Utilities;
using SFML.Window;

namespace LowRezJam
{
    public class ItemTorso : Item
    {
        #region Methods

        public ItemTorso(Vector2f position)
            : base(position)
        {
            Type = ItemType.TORSO;
            _offsetVector = new Vector2f(11, -9);
        }

        protected override void LoadGraphics()
        {
            _sprite = new SmartSprite("../GFX/item.png");
            _hudSprite = new SmartSprite("../GFX/item_torso.png");
        }

        #endregion Methods
    }
}
