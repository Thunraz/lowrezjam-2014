﻿using LowRezJam.Utilities;
using SFML.Window;

namespace LowRezJam
{
    public class ItemArms : Item
    {
        #region Methods

        public ItemArms(Vector2f position)
            : base(position)
        {
            Type = ItemType.ARMS;
            _offsetVector = new Vector2f(11, -3);
        }

        protected override void LoadGraphics()
        {
            _sprite = new SmartSprite("../GFX/item.png");
            _hudSprite = new SmartSprite("../GFX/item_arms.png");
        }

        #endregion Methods
    }
}
