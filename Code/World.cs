﻿using System;
using System.Collections.Generic;
using System.Linq;
using LowRezJam.Utilities;
using LowRezJam.Utilities.Particles;
using LowRezJam.Utilities.ScreenEffects;
using SFML.Audio;
using SFML.Graphics;
using SFML.Window;

namespace LowRezJam
{
    public class World : IGameObject
    {

        #region Fields

        private Animation _deathAnimation;
        private Player _player;
        private List<Enemy> _enemies;
        private Hud _hud;
        private List<Room> _rooms;
        private List<Corridor> _corridors;
        private List<Item> _items;
        private Music _backgroundMusic;

        public event NewGameRoundHandler NewGameRound;
        public delegate void NewGameRoundHandler();

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the game round.
        /// </summary>
        /// <value>
        /// The game round.
        /// </value>
        public int GameRound { get; set; }

        /// <summary>
        /// Gets the items.
        /// </summary>
        public List<Item> Items { get { return _items; } }

        /// <summary>
        /// Gets a value indicating whether this instance is game over.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is game over; otherwise, <c>false</c>.
        /// </value>
        public bool IsGameOver { get; private set; }

        #endregion Properties

        #region Methods

        public World()
        {
            InitGame();

            // Make sure the game round counter is incremented when the event is triggered
            NewGameRound += () => GameRound++;
        }

        public void InvokeNewGameRound()
        {
            NewGameRound();
        }

        public void GetInput()
        {
            _player.GetInput();
        }

        public void Update(TimeObject timeObject)
        {
            _player.Update(timeObject);

            if (_player.Health <= 0)
            {
                _backgroundMusic.Stop();
                _deathAnimation.Position = Config.GameView.Center - Config.GameView.Size / 2;
                _deathAnimation.Update(timeObject);

                if (!_deathAnimation.IsStarted && !_deathAnimation.IsFinished)
                    _deathAnimation.Start();
                else if (!_deathAnimation.IsStarted && _deathAnimation.IsFinished)
                    IsGameOver = true;
            }

            foreach (var enemy in _enemies)
            {
                enemy.Update(timeObject);
            }

            _hud.Update(timeObject);

            ScreenEffects.Update(timeObject);
            SpriteTrail.Update(timeObject);
            ParticleManager.Update(timeObject);

            if (_endLevelTimer > 0)
            {
                _endLevelTimer -= timeObject.ElapsedGameTime;
                if (_endLevelTimer < 0)
                {
                    NextLevel();
                    _endLevelTimer = 0;
                }
            }

            if (_enemies.Count == 0 && _endLevelTimer == 0)
            {
                Console.WriteLine("Starting transition to new Level");
                _endLevelTimer = Config.EndLevelTimerMax;
                ScreenEffects.GetDynamicEffect("fadeIn").StartEffect(Config.EndLevelTimerMax, 1.0f, Config.Colors[2], 3.0f);
            }
        }
        float _endLevelTimer = 0;

        private void NextLevel()
        {
            Console.WriteLine("Next Level");
            CreateWorld();

            _player.Position = FindRandomRoomPosition();
            ScreenEffects.ResetScreenEffects();
            ScreenEffects.GetDynamicEffect("fadeOut").StartEffect(Config.EndLevelTimerMax, 1.0f, Config.Colors[2], 2.0f);
        }

        public bool CanMoveToPosition(Vector2f newPosition)
        {
            // Check if the new position is inside a room
            foreach (var room in _rooms)
            {
                if (room.IsInside(newPosition))
                    return true;
            }

            // Check if the new position is inside a corridor
            foreach (var corridor in _corridors)
            {
                if (corridor.IsInside(newPosition))
                    return true;
            }

            return false;
        }

        public void Draw(RenderWindow rw)
        {
            ParticleManager.Draw(rw);

            foreach (var corridor in _corridors)
            {
                corridor.Draw(rw);
            }

            foreach (var room in _rooms)
            {
                room.Draw(rw);
            }

            // Draw items not in inventory
            foreach (var item in _items)
            {
                if (!item.IsInInventory)
                {
                    item.Draw(rw);
                }
            }

            _player.Draw(rw);

            foreach (var enemy in _enemies)
            {
                enemy.Draw(rw);
            }
            ScreenEffects.GetStaticEffect("vignette").Draw(rw);
            _hud.Draw(rw);


            // Draw items in inventory
            foreach (var kvp in _player.Inventory)
            {
                if (kvp.Value.IsInInventory)
                {
                    kvp.Value.Draw(rw);
                }
            }

            if (_player.Health <= 0)
            {
                _deathAnimation.Draw(rw);
            }

            ScreenEffects.Draw(rw);

        }

        public Enemy IsTileOccupied(Vector2f position)
        {
            return _enemies.FirstOrDefault(enemy => (int)enemy.Position.X == (int)position.X && (int)enemy.Position.Y == (int)position.Y);
        }

        public Vector2f? CheckCoordinatesForPlayer(List<Vector2f> coordinates)
        {
            foreach (var coordinate in coordinates)
            {
                if (_player.Position.X == coordinate.X && _player.Position.Y == coordinate.Y)
                {
                    return coordinate;
                }
            }

            return null;
        }

        public void FightPlayer(Enemy initiator)
        {
            _player.Hit(initiator.Damage);
            initiator.Hit(_player.Damage + _player.DamageModifier);

            if (initiator.Health <= 0)
            {
                KillEnemy(initiator);
            }
        }

        public Item CheckForItem(Vector2f position)
        {
            foreach (var item in _items)
            {
                if (item.Position.X == position.X && item.Position.Y == position.Y)
                {
                    return item;
                }
            }

            return null;
        }

        #region Privates

        private void KillEnemy(Enemy enemy)
        {
            _enemies.Remove(enemy);
            enemy.Dispose();

            // Check if the enemy dropped an item
            // 40% probability for an item drop
            if (RandomGenerator.Random.NextDouble() > 0.6)
            {
                // Get random item type
                var itemTypes = Enum.GetValues(typeof(ItemType));
                var itemType = (ItemType)itemTypes.GetValue(RandomGenerator.Random.Next(itemTypes.Length));

                switch (itemType)
                {
                    case ItemType.ARMS:
                        _items.Add(new ItemArms(enemy.Position));
                        break;
                    case ItemType.FEET:
                        _items.Add(new ItemFeet(enemy.Position));
                        break;
                    case ItemType.HEAD:
                        _items.Add(new ItemHead(enemy.Position));
                        break;
                    case ItemType.LEGS:
                        _items.Add(new ItemLegs(enemy.Position));
                        break;
                    case ItemType.TORSO:
                        _items.Add(new ItemTorso(enemy.Position));
                        break;
                }
            }
        }

        private void InitGame()
        {
            GameRound = 0;

            CreateWorld();

            _player = new Player(this, FindRandomRoomPosition());

            _deathAnimation = new Animation("../GFX/bloody.png", new IntRect(0, 0, 32, 32), 7, 0.1f, false);

            _hud = new Hud(this);



            _backgroundMusic = new Music("../SFX/music.ogg");
            _backgroundMusic.Loop = true;
            _backgroundMusic.Play();

        }

        private Vector2f FindRandomRoomPosition()
        {
            // Get random room
            var room = _rooms[RandomGenerator.Random.Next(0, _rooms.Count)];

            // Get random position inside the room
            var returnVector = new Vector2f();

            // This is some ugly stuff...
            returnVector.X = RandomGenerator.Random.Next((int)(room.Rect.Left / Config.TileSize), (int)((room.Rect.Left + room.Rect.Width) / Config.TileSize)) * Config.TileSize + 1;
            returnVector.Y = RandomGenerator.Random.Next((int)(room.Rect.Top / Config.TileSize), (int)((room.Rect.Top + room.Rect.Height) / Config.TileSize)) * Config.TileSize + 1;

            return returnVector;
        }

        private void CreateWorld()
        {
            _rooms = new List<Room>();
            _corridors = new List<Corridor>();
            _items = new List<Item>();

            for (var i = 0; i < Config.NumberOfRoomsToCreate; i++)
            {
                var rect = new FloatRect(
                    RandomGenerator.Random.Next(Config.RoomCreationArea.Left, Config.RoomCreationArea.Left + Config.RoomCreationArea.Width) * Config.TileSize,
                    RandomGenerator.Random.Next(Config.RoomCreationArea.Top, Config.RoomCreationArea.Top + Config.RoomCreationArea.Height) * Config.TileSize,
                    RandomGenerator.Random.Next(Config.MinRoomSize, Config.MaxRoomSize) * Config.TileSize,
                    RandomGenerator.Random.Next(Config.MinRoomSize, Config.MaxRoomSize) * Config.TileSize
                );

                var room = new Room(rect);
                var intersection = false;

                foreach (var presentRoom in _rooms)
                {
                    if (room.Intersects(presentRoom))
                    {
                        intersection = true;
                        break;
                    }
                }

                if (!intersection)
                {
                    _rooms.Add(room);
                    Console.WriteLine(
                        "Added room: X = {0}, Y = {1}, Width = {2}, Height = {3}",
                        room.Rect.Left,
                        room.Rect.Top,
                        room.Rect.Width,
                        room.Rect.Height
                    );

                    if (i >= 1)
                    {
                        _corridors.Add(new Corridor(_rooms[i - 1], _rooms[i]));
                    }
                }
                else
                {
                    i--;
                }
            }


            // Create some enemies
            _enemies = new List<Enemy>();
            for (var i = 0; i < 10; i++)
            {
                var enemy = new SimpleEnemy(this) { Position = FindRandomRoomPosition() };
                _enemies.Add(enemy);
            }
        }

        #endregion Privates

        #endregion Methods
    }
}
