﻿using System;
using System.Collections.Generic;
using LowRezJam.Utilities;
using SFML;
using SFML.Graphics;
using SFML.Window;

namespace LowRezJam
{
    public class Room : IGameObject
    {
        #region Fields

        protected SmartSprite _sprite;
        protected List<Color> _colorList;

        #endregion Fields

        #region Properties

        public FloatRect Rect { get; protected set; }

        public Vector2f Center
        {
            get
            {
                return new Vector2f(
                    (Rect.Left + Rect.Left + Rect.Width) / 2,
                    (Rect.Top + Rect.Top + Rect.Height) / 2
                );
            }
        }

        #endregion Properties

        #region Methods

        public Room(FloatRect rect)
        {
            Rect = rect;
            _colorList = ColorList.GetColorList(Color.Blue, Color.Cyan, Color.Green, Color.Magenta, Color.Red);

            try
            {
                LoadGraphics();
            }
            catch (LoadingFailedException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public bool Intersects(Room room)
        {
            var improvedRect = room.Rect;
            improvedRect.Left -= 1;
            improvedRect.Top -= 1;
            improvedRect.Width += 2;
            improvedRect.Height += 2;

            return Rect.Intersects(improvedRect);
        }

        public bool IsInside(Vector2f position)
        {
            if (position.X >= Rect.Left && position.X <= (Rect.Left + Rect.Width)
                    && position.Y >= Rect.Top && position.Y <= (Rect.Top + Rect.Height))
            {
                return true;
            }

            return false;
        }

        public void Update(TimeObject timeObject)
        {
        }

        public void Draw(RenderWindow rw)
        {
            _sprite.Draw(rw);
        }

        #region Privates

        private void LoadGraphics()
        {
            _sprite = new SmartSprite("../GFX/tile.png");
            _sprite.Sprite.TextureRect = new IntRect((int)Rect.Left, (int)Rect.Top, (int)Rect.Width, (int)Rect.Height);
            _sprite.Sprite.Texture.Repeated = true;

            // Don't forget to set the sprite's position
            _sprite.Position = new Vector2f(Rect.Left, Rect.Top);
        }

        #endregion Privates

        #endregion Methods
    }
}
