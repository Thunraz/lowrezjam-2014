﻿using SFML.Graphics;

namespace LowRezJam
{
    public static class Config
    {
        /// <summary>
        /// Gets the size of the tile.
        /// </summary>
        /// <value>
        /// The size of the tile.
        /// </value>
        public static float TileSize { get { return 3; } }

        /// <summary>
        /// Gets the player movement delay (the time between each "step").
        /// </summary>
        /// <value>
        /// The player movement delay.
        /// </value>
        public static float PlayerActionDelay { get { return 0.15f; } }

        /// <summary>
        /// Gets or sets the player initial health.
        /// </summary>
        /// <value>
        /// The player initial health.
        /// </value>
        public static int PlayerInitialHealth { get { return 3; } }

        /// <summary>
        /// Gets the player base damage.
        /// </summary>
        /// <value>
        /// The player base damage.
        /// </value>
        public static int PlayerBaseDamage { get { return 1; } }

        /// <summary>
        /// Gets the player base regeneration rate per second.
        /// </summary>
        /// <value>
        /// The player base regeneration rate.
        /// </value>
        public static float PlayerBaseRegenerationRate { get { return 0.05f; } }

        /// <summary>
        /// Gets the maximum size of the room.
        /// </summary>
        /// <value>
        /// The maximum size of the room.
        /// </value>
        public static int MaxRoomSize { get { return 10; } }

        /// <summary>
        /// Gets the minimum size of the room.
        /// </summary>
        /// <value>
        /// The minimum size of the room.
        /// </value>
        public static int MinRoomSize { get { return 6; } }

        /// <summary>
        /// Gets the number of rooms to create.
        /// </summary>
        /// <value>
        /// The number of rooms to create.
        /// </value>
        public static int NumberOfRoomsToCreate { get { return 5; } }

        /// <summary>
        /// Gets the room creation area.
        /// </summary>
        /// <value>
        /// The room creation area.
        /// </value>
        public static IntRect RoomCreationArea { get { return new IntRect(0, 0, 40, 40); } }

        /// <summary>
        /// Gets or sets the game view.
        /// </summary>
        /// <value>
        /// The game view.
        /// </value>
        public static View GameView { get; set; }

        /// <summary>
        /// The game's colors
        /// </summary>
        public static Color[] Colors =
        {
            new Color(0, 0, 0),         // Black
            new Color(255, 255, 255),   // White
            new Color(121, 40, 32),     // Brown
            new Color(134, 215, 223),   // Light blue
            new Color(170, 93, 182),    // Purple
            new Color(24, 130, 36),     // Dark green
            new Color(65, 48, 142),     // Dark blue
            new Color(190, 207, 113),   // Light green
            new Color(33, 33, 33),      // Dark grey
            new Color(235, 182, 138),   // Light Brown
            new Color(186, 105, 97)     // Brown
        };

        public static float EndLevelTimerMax { get { return 1.4f; } }
    }
}
