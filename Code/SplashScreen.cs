﻿using System;
using System.IO;
using LowRezJam.Utilities;
using SFML;
using SFML.Graphics;
using SFML.Window;

namespace LowRezJam
{
    public class SplashScreen
    {
        #region Fields

        private Sprite _logo;
        private Shape _overlay;
        private Shader _pixelate;
        private RenderStates _states;
        private float _timeToDisplay;

        private float _timeFadeIn;
        private readonly float _initialTimeFadeIn;
        private float _fadeAlphaIn = 1.0f;

        private float _timeFadeOut;
        private readonly float _initialTimeFadeOut;
        private float _fadeAlphaOut;

        private int _screenCounter;

        private readonly bool _doScreenShots;

        public event DoneHandler Done;
        public delegate void DoneHandler();

        #endregion Fields

        #region Methods

        public SplashScreen(float timeToDisplay = 2.0f, float fadeInTime = .25f, float fadeOutTime = .75f, bool doScreenShots = false)
        {
            _timeToDisplay = timeToDisplay;

            _initialTimeFadeIn = fadeInTime;
            _timeFadeIn = _initialTimeFadeIn;

            _initialTimeFadeOut = fadeOutTime;
            _timeFadeOut = fadeOutTime;

            _doScreenShots = doScreenShots;

            try
            {
                LoadGraphics();
            }
            catch (LoadingFailedException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void Draw(RenderWindow rw)
        {
            if (Shader.IsAvailable)
            {
                rw.Draw(_logo, _states);
            }
            else
            {
                rw.Draw(_logo);
            }
            rw.Draw(_overlay);

            if (_doScreenShots)
            {
                using (var image = rw.Capture())
                {
                    if (!Directory.Exists("../Screenshots"))
                    {
                        Directory.CreateDirectory("../Screenshots");
                    }

                    image.SaveToFile(string.Format("../Screenshots/splash_{0:000}.png", _screenCounter++));
                }
            }
        }

        public void Update(TimeObject t)
        {
            _timeFadeIn -= t.ElapsedGameTime;

            if (_timeFadeIn <= 0.0f)
            {
                _timeToDisplay -= t.ElapsedGameTime;

                if (Math.Abs(_timeFadeOut - _initialTimeFadeOut) < 0.01f)
                {
                    _overlay.FillColor = new Color(0, 0, 0, 0);
                }

                if (_timeToDisplay <= 0.0f)
                {
                    _timeFadeOut -= t.ElapsedGameTime;

                    if (_overlay.FillColor.A == byte.MaxValue)
                    {
                        // We're finished here
                        // Execute the event and return
                        Done();
                        return;
                    }

                    FadeLogoOut();
                }
            }
            else
            {
                FadeLogoIn();
            }
        }

        #region Privates

        private void LoadGraphics()
        {
            _logo = new Sprite(new Texture("../GFX/runvs.png"));
            _overlay = new RectangleShape(new Vector2f(512, 512));
            _overlay.FillColor = new Color(0, 0, 0, 255);

            if (Shader.IsAvailable)
            {
                _pixelate = new Shader(null, "../GFX/pixelate.frag");
                _pixelate.SetParameter("tex", _logo.Texture);
                _pixelate.SetParameter("strength", 0.0f);
                _states = new RenderStates(_pixelate);
            }
        }

        /// <summary>
        /// Fades the logo in.
        /// </summary>
        private void FadeLogoIn()
        {
            _fadeAlphaIn = _timeFadeIn / _initialTimeFadeIn;
            _overlay.FillColor = new Color(0, 0, 0, (byte)(_fadeAlphaIn * 255));
        }

        /// <summary>
        /// Fades the logo out.
        /// </summary>
        private void FadeLogoOut()
        {
            _fadeAlphaOut = 1.0f - (_timeFadeOut / _initialTimeFadeOut);

            if (_fadeAlphaOut > 1.0f)
            {
                _fadeAlphaOut = 1.0f;
            }

            if (Shader.IsAvailable)
            {
                _pixelate.SetParameter("strength", _fadeAlphaOut * 50.0f);
            }

            _overlay.FillColor = new Color(0, 0, 0, (byte)(_fadeAlphaOut * 255));
        }

        #endregion Privates

        #endregion Methods
    }
}
