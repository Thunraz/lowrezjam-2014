﻿
namespace LowRezJam
{
    interface IHittable
    {
        /// <summary>
        /// Hits the Hittable.
        /// </summary>
        /// <param name="damage">The damage.</param>
        void Hit(float damage);
    }
}
