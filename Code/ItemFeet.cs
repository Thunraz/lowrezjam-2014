﻿using LowRezJam.Utilities;
using SFML.Window;

namespace LowRezJam
{
    public class ItemFeet : Item
    {
        #region Methods

        public ItemFeet(Vector2f position)
            : base(position)
        {
            Type = ItemType.FEET;
            _offsetVector = new Vector2f(11, 9);
        }

        protected override void LoadGraphics()
        {
            _sprite = new SmartSprite("../GFX/item.png");
            _hudSprite = new SmartSprite("../GFX/item_feet.png");
        }

        #endregion Methods
    }
}
