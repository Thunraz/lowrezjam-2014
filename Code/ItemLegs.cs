﻿using LowRezJam.Utilities;
using SFML.Window;

namespace LowRezJam
{
    public class ItemLegs : Item
    {
        #region Methods

        public ItemLegs(Vector2f position)
            : base(position)
        {
            Type = ItemType.LEGS;
            _offsetVector = new Vector2f(11, 3);
        }

        protected override void LoadGraphics()
        {
            _sprite = new SmartSprite("../GFX/item.png");
            _hudSprite = new SmartSprite("../GFX/item_legs.png");
        }

        #endregion Methods
    }
}
