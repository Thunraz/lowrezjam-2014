﻿using LowRezJam.Utilities;
using SFML.Graphics;

namespace LowRezJam
{
    interface IGameObject
    {
        /// <summary>
        /// Updates the game object.
        /// </summary>
        /// <param name="timeObject">The time object.</param>
        void Update(TimeObject timeObject);

        /// <summary>
        /// Draws the game object.
        /// </summary>
        /// <param name="rw">The rw.</param>
        void Draw(RenderWindow rw);
    }
}
