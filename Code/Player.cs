﻿using System;
using System.Collections.Generic;
using LowRezJam.Utilities;
using LowRezJam.Utilities.ScreenEffects;
using SFML;
using SFML.Audio;
using SFML.Graphics;
using SFML.Window;

namespace LowRezJam
{
    public class Player : IHittable, IGameObject
    {

        #region Properties

        /// <summary>
        /// Gets the health.
        /// </summary>
        /// <value>
        /// The health.
        /// </value>
        public float Health { get; private set; }

        /// <summary>
        /// Gets the damage.
        /// </summary>
        /// <value>
        /// The damage.
        /// </value>
        public float Damage { get; private set; }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        public Vector2f Position { get; set; }

        /// <summary>
        /// Gets a value indicating whether the player [is alive].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is alive]; otherwise, <c>false</c>.
        /// </value>
        public bool IsAlive { get { return Health > 0; } }

        /// <summary>
        /// The player's inventory
        /// </summary>
        public Dictionary<ItemType, Item> Inventory { get; private set; }

        /// <summary>
        /// Gets the damage modifier.
        /// </summary>
        /// <value>
        /// The damage modifier.
        /// </value>
        public float DamageModifier
        {
            get
            {
                float modifier = 0.0f;
                foreach (var item in Inventory)
                {
                    modifier += item.Value.DamageModifier;
                }

                return modifier;
            }
        }

        /// <summary>
        /// Gets the armor modifier.
        /// </summary>
        /// <value>
        /// The armor modifier.
        /// </value>
        public float ArmorModifier
        {
            get
            {
                float modifier = 0.0f;
                foreach (var item in Inventory)
                {
                    modifier += item.Value.ArmorModifier;
                }

                return modifier;
            }
        }

        #endregion Properties

        #region Fields

        private SmartSprite _sprite;
        //private SmartSprite _spriteHealthBar;
        private RectangleShape _shapeHealthBar1;
        private RectangleShape _shapeHealthBar2;
        private RectangleShape _shapeHealthBar3;

        private readonly Dictionary<Keyboard.Key, Action> _actionMap;
        private float _actionTimer = Config.PlayerActionDelay;
        private float _initialHealth = Config.PlayerInitialHealth;
        private World _world;
        private PlannedAction _plannedAction = null;

        private SoundBuffer _hitSoundBuffer;
        private Sound _hitSound;

        private SoundBuffer _splatterSoundBuffer;
        private Sound _splatterSound;

        #endregion Fields

        #region Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="world">The world.</param>
        public Player(World world, Vector2f startPosition)
        {
            _world = world;
            _world.NewGameRound += NewGameRound;

            Inventory = new Dictionary<ItemType, Item>();

            _actionMap = new Dictionary<Keyboard.Key, Action>();
            Position = startPosition;
            Health = Config.PlayerInitialHealth;
            Damage = Config.PlayerBaseDamage;

            try
            {
                LoadGraphics();
                _hitSoundBuffer = new SoundBuffer("../SFX/hit.ogg");
                _hitSound = new Sound(_hitSoundBuffer);
                _hitSound.Volume = 25.0f;

                _splatterSoundBuffer = new SoundBuffer("../SFX/splatter.ogg");
                _splatterSound = new Sound(_splatterSoundBuffer);
                _splatterSound.Volume = 50.0f;
            }
            catch (LoadingFailedException e)
            {
                Console.WriteLine(e);
                throw;
            }

            SetupActionMap();
            RecalculateHealthBar();
        }

        /// <summary>
        /// Handles the input
        /// </summary>
        public void GetInput()
        {
            MapInputToActions();
        }

        /// <summary>
        /// Updates the player.
        /// </summary>
        /// <param name="timeObject">The time object.</param>
        public void Update(TimeObject timeObject)
        {
            if (_actionTimer > 0.0f)
            {
                _actionTimer -= timeObject.ElapsedRealTime;
            }

            _sprite.Position = Position;
            //_spriteHealthBar.Position = Position - new Vector2f(1, 1);
            _shapeHealthBar1.Position = Position - new Vector2f(1, 1);
            _shapeHealthBar2.Position = Position - new Vector2f(0, 1);
            _shapeHealthBar3.Position = Position - new Vector2f(-1, 1);

            _sprite.Update(timeObject);
        }

        /// <summary>
        /// Draws the player on the specified [RenderWindow].
        /// </summary>
        /// <param name="rw">The rw.</param>
        public void Draw(RenderWindow rw)
        {
            // Center the view on the player except when he's out of bounds
            Config.GameView.Center = Position;
            rw.SetView(Config.GameView);
            ScreenEffects.GlobalCameraOffset = Config.GameView.Center - new Vector2f(16, 16);

            _sprite.Draw(rw);
            rw.Draw(_shapeHealthBar1);
            rw.Draw(_shapeHealthBar2);
            rw.Draw(_shapeHealthBar3);
        }

        public void Hit(float damage)
        {
            Console.Write("Game Round {0}: Player hit! Health {1}", _world.GameRound, Health);

            Health -= damage - ArmorModifier;

            _hitSound.Play();

            if (Health <= 0.0f)
            {
                Health = 0.0f;
                _splatterSound.Play();
            }

            RecalculateHealthBar();
            Console.WriteLine("->" + Health);

            _sprite.Flash(Config.Colors[2], 0.2f);
        }

        private void RecalculateHealthBar()
        {
            uint numberOfFilledSquares = (uint)(Math.Floor(Health / _initialHealth * 3.0f));
            //System.Console.WriteLine(numberOfFilledSquares);
            float healthFactor = _initialHealth / 3.0f;
            if (Health <= 0)
            {
                _shapeHealthBar1.FillColor = new Color(0, 0, 0, 0);
                _shapeHealthBar2.FillColor = new Color(0, 0, 0, 0);
                _shapeHealthBar3.FillColor = new Color(0, 0, 0, 0);
                return;
            }
            if (numberOfFilledSquares == 0)
            {
                float fillFraction = Health / healthFactor;
                byte redpart = (byte)(55 + 200.0f * (1.0f - fillFraction));
                byte greenpart = (byte)(55 + 200.0f * fillFraction);
                _shapeHealthBar1.FillColor = new Color(redpart, greenpart, 50);

                // health bar 2 and 3 are not visible
                _shapeHealthBar2.FillColor = new Color(0, 0, 0, 0);
                _shapeHealthBar3.FillColor = new Color(0, 0, 0, 0);
            }
            else if (numberOfFilledSquares == 1)
            {
                _shapeHealthBar1.FillColor = new Color(50, 255, 50, 255);

                float fillFraction = (Health - 1.0f) / healthFactor;
                byte redpart = (byte)(55 + 200.0f * (1.0f - fillFraction));
                byte greenpart = (byte)(55 + 200.0f * fillFraction);
                _shapeHealthBar2.FillColor = new Color(redpart, greenpart, 50);

                // health bar 2 and 3 are not visible

                _shapeHealthBar3.FillColor = new Color(0, 0, 0, 0);
            }
            else if (numberOfFilledSquares == 2)
            {
                _shapeHealthBar1.FillColor = new Color(50, 255, 50, 255);
                _shapeHealthBar2.FillColor = new Color(50, 255, 50, 255);

                float fillFraction = (Health - 2.0f) / healthFactor;
                byte redpart = (byte)(55 + 200.0f * (1.0f - fillFraction));
                byte greenpart = (byte)(55 + 200.0f * fillFraction);
                _shapeHealthBar3.FillColor = new Color(redpart, greenpart, 50);
            }
            else
            {
                _shapeHealthBar1.FillColor = new Color(50, 255, 50, 255);
                _shapeHealthBar2.FillColor = new Color(50, 255, 50, 255);
                _shapeHealthBar3.FillColor = new Color(50, 255, 50, 255);
            }
        }

        #region Privates

        private void SetupActionMap()
        {
            _actionMap.Add(Keyboard.Key.Left, () => PlanAction(ActionType.WALK, new Vector2f(-Config.TileSize, 0)));
            _actionMap.Add(Keyboard.Key.A, () => PlanAction(ActionType.WALK, new Vector2f(-Config.TileSize, 0)));
            _actionMap.Add(Keyboard.Key.Right, () => PlanAction(ActionType.WALK, new Vector2f(Config.TileSize, 0)));
            _actionMap.Add(Keyboard.Key.D, () => PlanAction(ActionType.WALK, new Vector2f(Config.TileSize, 0)));
            _actionMap.Add(Keyboard.Key.Up, () => PlanAction(ActionType.WALK, new Vector2f(0, -Config.TileSize)));
            _actionMap.Add(Keyboard.Key.W, () => PlanAction(ActionType.WALK, new Vector2f(0, -Config.TileSize)));
            _actionMap.Add(Keyboard.Key.Down, () => PlanAction(ActionType.WALK, new Vector2f(0, Config.TileSize)));
            _actionMap.Add(Keyboard.Key.S, () => PlanAction(ActionType.WALK, new Vector2f(0, Config.TileSize)));
            _actionMap.Add(Keyboard.Key.Space, () => PlanAction(ActionType.WAIT, null));
        }

        private void NewGameRound()
        {
            if (_plannedAction == null)
            {
                _plannedAction = new PlannedAction(ActionType.WAIT);
            }
            switch (_plannedAction.Action)
            {
                case ActionType.WAIT:
                    var item = _world.CheckForItem(Position);
                    if (item != null)
                    {
                        PickupItem(item);
                    }
                    else
                    {
                        Wait();
                    }
                    break;
                case ActionType.WALK:
                default:
                    Move((Vector2f)_plannedAction.Parameter);
                    break;
            }

            _actionTimer += Config.PlayerActionDelay;
        }

        private void PickupItem(Item item)
        {
            Console.WriteLine("Game Round {0}: Picking up Item {1}", _world.GameRound, item);

            if (Inventory.ContainsKey(item.Type))
            {
                Inventory[item.Type].Dispose();
                Inventory[item.Type] = item;
            }
            else
            {
                Inventory.Add(item.Type, item);
            }

            _world.Items.Remove(item);
            Inventory[item.Type].IsInInventory = true;
        }

        private void Wait()
        {
            if (Health < _initialHealth)
            {
                Console.Write("Game Round {0}: Healed a bit... {1}", _world.GameRound, Health);
                Health += Config.PlayerBaseRegenerationRate;
                Console.WriteLine("->{0}", Health);

                RecalculateHealthBar();
            }
            else
            {
                Health = _initialHealth;
            }
        }

        private void PlanAction(ActionType action, object parameter)
        {
            if (_actionTimer <= 0.0f && Health > 0)
            {
                _plannedAction = new PlannedAction(action, parameter);

                _world.InvokeNewGameRound();
            }
        }

        private void Move(Vector2f direction)
        {
            // Check if the player can move to the position
            if (!_world.CanMoveToPosition(Position + direction))
            {
                Wait();
                return;
            }

            // Check if the tile is occupied by an enemy
            var occupyingEnemy = _world.IsTileOccupied(Position + direction);
            if (occupyingEnemy != null)
            {
                Hit(occupyingEnemy.Damage);
                occupyingEnemy.Hit(Damage);
            }
            else
            {
                Position += direction;
            }
        }

        private void MapInputToActions()
        {

            foreach (var kvp in _actionMap)
            {
                if (Keyboard.IsKeyPressed(kvp.Key))
                {
                    // Execute the saved callback
                    kvp.Value();
                }
            }
        }

        private void LoadGraphics()
        {
            _sprite = new SmartSprite("../GFX/player.png");
            //_spriteHealthBar = new SmartSprite("../GFX/healthbar.png");
            _shapeHealthBar1 = new RectangleShape(new Vector2f(1, 1));
            _shapeHealthBar1.FillColor = Color.Red;
            _shapeHealthBar2 = new RectangleShape(new Vector2f(1, 1));
            _shapeHealthBar2.FillColor = Color.Red;
            _shapeHealthBar3 = new RectangleShape(new Vector2f(1, 1));
            _shapeHealthBar3.FillColor = Color.Red;
        }

        #endregion Privates

        #endregion Methods

        #region Inner classes, enums

        private enum ActionType
        {
            WALK,
            WAIT
        }

        private class PlannedAction
        {
            public ActionType Action { get; set; }
            public object Parameter;

            public PlannedAction(ActionType actionType, object parameter = null)
            {
                Action = actionType;
                Parameter = parameter;
            }
        }

        #endregion Inner classes, enums

    }
}
