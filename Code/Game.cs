﻿using System;
using LowRezJam.Utilities;
using LowRezJam.Utilities.ScreenEffects;
using SFML.Graphics;
using SFML.Window;

namespace LowRezJam
{
    class Game
    {

        #region Fields

        private State _gameState;
        World _myWorld;

        float _timeTilNextInput = 0.0f;
        SmartSprite _menuSprite;
        SmartSprite _creditsSprite;
        SmartSprite _scoreSprite;

        public bool CanBeQuit { get; set; }

        #endregion Fields

        #region Methods

        public Game()
        {
            // Predefine game state to menu
            _gameState = State.Menu;

            SmartSprite._scaleVector = new Vector2f(1.0f, 1.0f);
            ScreenEffects.Init(new Vector2u(32, 32));
            try
            {
                _menuSprite = new SmartSprite("../GFX/menu.png");
                _creditsSprite = new SmartSprite("../GFX/credits.png");
                _scoreSprite = new SmartSprite("../GFX/score.png");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void GetInput()
        {
            if (_timeTilNextInput < 0.0f)
            {
                switch (_gameState)
                {
                    case State.Menu:
                        GetInputMenu();
                        break;
                    case State.Game:
                        _myWorld.GetInput();
                        break;
                    case State.Credits:
                    case State.Score:
                        GetInputCreditsScore();
                        break;
                }
            }
        }

        private void GetInputMenu()
        {
            if (Keyboard.IsKeyPressed(Keyboard.Key.Return))
            {
                StartGame();
            }

            if (Keyboard.IsKeyPressed(Keyboard.Key.C))
            {
                ChangeGameState(State.Credits);
            }

        }

        private void GetInputCreditsScore()
        {
            if (Keyboard.IsKeyPressed(Keyboard.Key.Escape) || Keyboard.IsKeyPressed(Keyboard.Key.Return) || Keyboard.IsKeyPressed(Keyboard.Key.Space))
            {
                ChangeGameState(State.Menu, 1.0f);
            }
        }

        public void Update(float deltaT)
        {
            if (_timeTilNextInput >= 0.0f)
            {
                _timeTilNextInput -= deltaT;
            }

            CanBeQuit = false;
            if (_gameState == State.Game)
            {
                if (!_myWorld.IsGameOver)
                {
                    _myWorld.Update(Timing.Update(deltaT));
                }
                else
                {
                    ChangeGameState(State.Score);
                }

            }
            else if (_gameState == State.Menu && this._timeTilNextInput <= 0.0f)
            {
                CanBeQuit = true;
            }
        }

        public void Draw(RenderWindow rw)
        {
            rw.Clear();
            switch (_gameState)
            {
                case State.Menu:
                    DrawMenu(rw);
                    break;
                case State.Game:
                    _myWorld.Draw(rw);
                    break;
                case State.Credits:
                    DrawCredits(rw);
                    break;
                case State.Score:
                    DrawScore(rw);
                    break;
            }
        }

        private void DrawMenu(RenderWindow rw)
        {
            var view = rw.GetView();
            _menuSprite.Position = view.Center - view.Size / 2;
            _menuSprite.Draw(rw);
        }

        private void DrawCredits(RenderWindow rw)
        {
            var view = rw.GetView();
            _creditsSprite.Position = view.Center - view.Size / 2;
            _creditsSprite.Draw(rw);
        }

        private void DrawScore(RenderWindow rw)
        {
            var view = rw.GetView();
            _scoreSprite.Position = view.Center - view.Size / 2;
            _scoreSprite.Draw(rw);
        }

        private void ChangeGameState(State newState, float inputdeadTime = 0.5f)
        {
            this._gameState = newState;
            _timeTilNextInput = inputdeadTime;
        }

        private void StartGame()
        {
            _myWorld = new World();
            ChangeGameState(State.Game, 0.1f);
        }

        #endregion Methods

        #region Subclasses/Enums

        private enum State
        {
            Menu,
            Game,
            Score,
            Credits
        }

        #endregion Subclasses/Enums
    }
}
