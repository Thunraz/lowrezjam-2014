﻿using System;
using LowRezJam.Utilities;
using SFML.Graphics;
using SFML.Window;

namespace LowRezJam
{
    class Program
    {
        private static bool _hasFocus = true;

        #region Event handlers

        private static void OnClose(object sender, EventArgs e)
        {
            // Close the window when OnClose event is received
            var window = (RenderWindow)sender;
            window.Close();
        }

        #endregion Event handlers

        static void Main()
        {
            var applicationWindow = new RenderWindow(new VideoMode(512, 512, 32), "Sbläddah");//, Styles.Close);

            // Apply view
            Config.GameView = new View(new FloatRect(0, 0, 32, 32));
            Config.GameView.Size = new Vector2f(512, 512);
            Config.GameView.Zoom(1 / 16f);

            applicationWindow.SetFramerateLimit(60);
            applicationWindow.SetVerticalSyncEnabled(true);

            applicationWindow.Closed += OnClose;

            var myGame = new Game();
#if DEBUG
            var splashDone = true;
            applicationWindow.SetView(Config.GameView);
#else
            var splashDone = false;
            applicationWindow.LostFocus += (o, e) => _hasFocus = false;
            applicationWindow.GainedFocus += (o, e) => _hasFocus = true;
#endif
            var splash = new SplashScreen();
            splash.Done += () =>
            {
                splashDone = true;
                applicationWindow.SetView(Config.GameView);
            };

            Utilities.Mouse.Window = applicationWindow;

            int startTime = Environment.TickCount;
            int endTime = startTime;
            float time = 16.7f / 1000.0f; // 60 fps -> 16.7 ms per frame

            while (applicationWindow.IsOpen())
            {
                if (startTime != endTime)
                {
                    time = (endTime - startTime) / 1000.0f;
                }
                startTime = Environment.TickCount;

                applicationWindow.DispatchEvents();

                if (splashDone)
                {
                    if (_hasFocus)
                    {
                        myGame.GetInput();
                        if (myGame.CanBeQuit)
                        {
                            if (Keyboard.IsKeyPressed(Keyboard.Key.Escape))
                            {
                                applicationWindow.Close();
                            }
                        }

                        Utilities.Mouse.Update();
                    }

                    myGame.Update(time);

                    myGame.Draw(applicationWindow);
                }
                else
                {
                    splash.Update(Timing.Update(time));
                    splash.Draw(applicationWindow);
                }

                applicationWindow.Display();
                endTime = Environment.TickCount;
            }
        }
    }
}
