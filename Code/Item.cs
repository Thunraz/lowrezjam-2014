﻿using System;
using LowRezJam.Utilities;
using SFML;
using SFML.Graphics;
using SFML.Window;

namespace LowRezJam
{
    public abstract class Item : IGameObject, IDisposable
    {
        #region Fields

        protected SmartSprite _sprite;
        protected SmartSprite _hudSprite;
        protected Vector2f _offsetVector;

        //shapes to display inventory values
        private RectangleShape _damageShape;
        private RectangleShape _armorShape;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the item type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public ItemType Type { get; protected set; }

        /// <summary>
        /// Gets the position.
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        public Vector2f Position { get { return _sprite.Position + new Vector2f(1, 1); } }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is in inventory.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is in inventory; otherwise, <c>false</c>.
        /// </value>
        public bool IsInInventory { get; set; }

        /// <summary>
        /// Gets or sets the damage modifier.
        /// </summary>
        /// <value>
        /// The damage modifier.
        /// </value>
        public float DamageModifier { get; protected set; }

        /// <summary>
        /// Gets or sets the armor modifier.
        /// </summary>
        /// <value>
        /// The armor modifier.
        /// </value>
        public float ArmorModifier { get; protected set; }

        #endregion Properties

        #region Methods

        public Item(Vector2f position)
        {
            try
            {
                LoadGraphics();
                _sprite.Position = position - new Vector2f(1, 1);
                _damageShape = new RectangleShape(new Vector2f(2,1));
                _armorShape = new RectangleShape(new Vector2f(2, 1));
            }
            catch (LoadingFailedException e)
            {
                Console.WriteLine(e);
                throw;
            }

            DamageModifier = (float)RandomGenerator.Random.NextDouble() - 0.5f;
            ArmorModifier = (float)RandomGenerator.Random.NextDouble() - 0.5f;
        }

        public void Update(TimeObject timeObject)
        {
            throw new NotImplementedException();
        }

        public void Draw(RenderWindow rw)
        {
            if (IsInInventory)
            {
                var view = rw.GetView();
                _hudSprite.Position = view.Center + _offsetVector;
                _hudSprite.Draw(rw);

                DrawHudInfo(rw);
            }
            else
            {
                _sprite.Draw(rw);
            }
        }

        private Color GetHUDColorFromValue(float value)
        {
            byte r = 100;
            byte g = 100;
            byte b = 100;
            
            if (value > 0)
            {
                float factor = (float)value / 0.5f;
                g += (byte)(155.0f * factor);
            }
            else if (value < 0)
            {
                float factor = -(float)value / 0.5f;
                r += (byte)(155.0f * factor);
            }

            return new Color(r,g,b);
        }

        private void DrawHudInfo(RenderWindow rw)
        {
            _damageShape.FillColor = GetHUDColorFromValue(DamageModifier);
            _armorShape.FillColor = GetHUDColorFromValue(ArmorModifier);
            var view = rw.GetView();
            _armorShape.Position = view.Center + _offsetVector + new Vector2f(0, 5);
            _damageShape.Position = view.Center + _offsetVector + new Vector2f(3, 5);

            rw.Draw(_damageShape);
            rw.Draw(_armorShape);
        }

        protected abstract void LoadGraphics();

        public void Dispose()
        {
            _sprite.Sprite.Dispose();
            _hudSprite.Sprite.Dispose();

            _sprite = null;
            _hudSprite = null;
        }

        public override string ToString()
        {
            return string.Format("{0} Item, Damage {1}, Armor {2}", Type, DamageModifier, ArmorModifier);
        }

        #endregion Methods
    }

    public enum ItemType
    {
        HEAD,
        TORSO,
        ARMS,
        LEGS,
        FEET
    }
}
