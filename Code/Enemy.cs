﻿using System;
using System.Collections.Generic;
using LowRezJam.Utilities;
using SFML.Graphics;
using SFML.Window;

namespace LowRezJam
{
    public abstract class Enemy : IHittable, IDisposable, IGameObject
    {
        #region Fields

        protected static int _globalEnemyNumber = 0;

        protected SmartSprite _sprite;
        protected SmartSprite _spriteHealthBar;
        protected World _world;
        protected float _initialHealth;
        protected int _searchRadius;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the health.
        /// </summary>
        /// <value>
        /// The health.
        /// </value>
        public float Health { get; protected set; }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        public Vector2f Position { get; set; }

        /// <summary>
        /// Gets a value indicating whether the enemy [is alive].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is alive]; otherwise, <c>false</c>.
        /// </value>
        public bool IsAlive { get { return Health > 0; } }

        /// <summary>
        /// Gets the damage.
        /// </summary>
        /// <value>
        /// The damage.
        /// </value>
        public float Damage { get; protected set; }

        /// <summary>
        /// Gets or sets the enemy number.
        /// </summary>
        /// <value>
        /// The enemy number.
        /// </value>
        public int EnemyNumber { get; protected set; }

        #endregion Properties

        #region Methods

        public void Draw(RenderWindow rw)
        {
            _sprite.Draw(rw);
            _spriteHealthBar.Draw(rw);
        }

        protected Enemy(World world)
        {
            _world = world;
            _world.NewGameRound += NewGameRound;
            EnemyNumber = _globalEnemyNumber++;
        }

        public void Dispose()
        {
            _world.NewGameRound -= NewGameRound;
        }

        protected void SearchPlayer()
        {
            // Check some coordinates
            var coordinates = new List<Vector2f>();

            for (int radius = 1; radius <= _searchRadius; radius++)
            {
                coordinates.Add(new Vector2f(Position.X - radius * Config.TileSize, Position.Y - radius * Config.TileSize));
                coordinates.Add(new Vector2f(Position.X - radius * Config.TileSize, Position.Y));
                coordinates.Add(new Vector2f(Position.X - radius * Config.TileSize, Position.Y + radius * Config.TileSize));
                coordinates.Add(new Vector2f(Position.X, Position.Y - radius * Config.TileSize));
                coordinates.Add(new Vector2f(Position.X, Position.Y + radius * Config.TileSize));
                coordinates.Add(new Vector2f(Position.X + radius * Config.TileSize, Position.Y - radius * Config.TileSize));
                coordinates.Add(new Vector2f(Position.X + radius * Config.TileSize, Position.Y));
                coordinates.Add(new Vector2f(Position.X + radius * Config.TileSize, Position.Y + radius * Config.TileSize));
            }

            var checkedCoordinate = _world.CheckCoordinatesForPlayer(coordinates);
            if (checkedCoordinate.HasValue)
            {
                var coord = checkedCoordinate.Value;
                Console.WriteLine(
                    "Game Round {0}: Enemy{1} has found the player at (X = {2}, Y = {3}), own Position (X = {4}, Y = {5})",
                    _world.GameRound,
                    EnemyNumber,
                    coord.X,
                    coord.Y,
                    Position.X,
                    Position.Y
                );

                var vec = new Vector2f(
                    Math.Sign(coord.X - Position.X) * Config.TileSize,
                    Math.Sign(coord.Y - Position.Y) * Config.TileSize
                );

                var newPos = Position + vec;

                if (newPos.X == coord.X && newPos.Y == coord.Y)
                {
                    // We would sit on the player. We don't want that.
                    // Hit him instead.
                    _world.FightPlayer(this);
                }
                else
                {
                    Position = newPos;
                }
            }
        }

        public abstract void Update(TimeObject timeObject);
        public abstract void Hit(float damage);
        protected abstract void NewGameRound();

        #endregion Methods
    }
}
