﻿using System;
using System.Collections.Generic;
using LowRezJam.Utilities;
using SFML;
using SFML.Graphics;
using SFML.Window;

namespace LowRezJam
{
    public class Corridor : IGameObject
    {
        #region Fields

        protected List<SmartSprite> _sprites;

        #endregion Fields

        #region Properties

        public List<FloatRect> Rects { get; protected set; }

        #endregion Properties

        #region Methods

        public Corridor(Room first, Room second)
        {
            CreateCorridors(first.Center, second.Center);

            try
            {
                LoadGraphics();
            }
            catch (LoadingFailedException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public bool IsInside(Vector2f position)
        {
            foreach (var rect in Rects)
            {
                if (position.X >= rect.Left && position.X <= (rect.Left + rect.Width)
                        && position.Y >= rect.Top && position.Y <= (rect.Top + rect.Height))
                {
                    return true;
                }
            }

            return false;
        }

        public void Update(TimeObject timeObject)
        {
            throw new NotImplementedException();
        }

        public void Draw(RenderWindow rw)
        {
            foreach (var sprite in _sprites)
            {
                sprite.Draw(rw);
            }
        }

        #region Privates

        private void CreateCorridors(Vector2f first, Vector2f second)
        {
            Rects = new List<FloatRect>();

            // Horizontal corridor
            var rect = new FloatRect();
            rect.Left = (int)Math.Min(first.X, second.X);
            rect.Width = (int)Math.Max(first.X, second.X) - rect.Left;
            rect.Top = (int)(first.Y / Config.TileSize) * Config.TileSize;
            rect.Height = 1 * Config.TileSize;
            Rects.Add(rect);

            // Vertical corridor
            rect = new FloatRect();
            rect.Left = (int)(second.X / Config.TileSize) * Config.TileSize;
            rect.Width = 1 * Config.TileSize;
            rect.Top = (int)Math.Min(first.Y, second.Y);
            rect.Height = (int)Math.Max(first.Y, second.Y) - rect.Top;
            Rects.Add(rect);
        }

        private void LoadGraphics()
        {
            _sprites = new List<SmartSprite>();

            foreach (var rect in Rects)
            {
                var sprite = new SmartSprite("../GFX/tile.png");
                sprite.Sprite.TextureRect = new IntRect((int)rect.Left, (int)rect.Top, (int)rect.Width, (int)rect.Height);
                sprite.Sprite.Texture.Repeated = true;

                sprite.Position = new Vector2f(rect.Left, rect.Top);
                _sprites.Add(sprite);
            }
        }

        #endregion Privates

        #endregion Methods
    }
}
