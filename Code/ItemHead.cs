﻿using LowRezJam.Utilities;
using SFML.Window;

namespace LowRezJam
{
    public class ItemHead : Item
    {
        #region Methods

        public ItemHead(Vector2f position)
            : base(position)
        {
            Type = ItemType.HEAD;
            _offsetVector = new Vector2f(11, -15);
        }

        protected override void LoadGraphics()
        {
            _sprite = new SmartSprite("../GFX/item.png");
            _hudSprite = new SmartSprite("../GFX/item_head.png");
        }

        #endregion Methods
    }
}
