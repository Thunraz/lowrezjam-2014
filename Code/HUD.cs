﻿using System;
using LowRezJam.Utilities;
using SFML;
using SFML.Graphics;
using SFML.Window;

namespace LowRezJam
{
    public class Hud
    {
        #region Fields

        private World _world;
        private SmartSprite _sprite;

        #endregion Fields

        #region Properties

        public Vector2f Position { get; set; }

        #endregion Properties

        #region Methods

        public Hud(World world)
        {
            _world = world;

            try
            {
                LoadGraphics();
            }
            catch (LoadingFailedException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void Draw(RenderWindow rw)
        {
            var view = rw.GetView();

            _sprite.Position = view.Center + new Vector2f(11, -16);
            _sprite.Draw(rw);
        }

        public void Update(TimeObject timeObject)
        {

        }

        private void LoadGraphics()
        {
            _sprite = new SmartSprite("../GFX/hud.png");
        }

        #endregion Methods
    }
}
