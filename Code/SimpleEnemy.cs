﻿using System;
using LowRezJam.Utilities;
using SFML.Graphics;
using SFML.Window;

namespace LowRezJam
{
    class SimpleEnemy : Enemy
    {
        #region Fields

        #endregion Fields

        #region Properties

        #endregion Properties

        #region Methods

        public SimpleEnemy(World world)
            : base(world)
        {
            Health = _initialHealth = 1.0f;
            Damage = 0.75f;
            _searchRadius = 3;

            LoadGraphics();
        }

        public override void Update(TimeObject timeObject)
        {
            _sprite.Position = Position;
            _spriteHealthBar.Position = Position - new Vector2f(1, 1);
            _sprite.Update(timeObject);
        }

        public override void Hit(float damage)
        {
            Console.Write("Game Round {0}: Enemy{1} hit! Health {2}", _world.GameRound, EnemyNumber, Health);
            Health -= damage;
            _spriteHealthBar.Sprite.TextureRect = new IntRect(0, 0, (int)Math.Round(Health / _initialHealth * Config.TileSize), 1);
            Console.WriteLine("->" + Health);
            _sprite.Flash(Config.Colors[2], 0.2f);
        }

        #region Private/Protected

        protected override void NewGameRound()
        {
            SearchPlayer();
        }

        protected void LoadGraphics()
        {
            _sprite = new SmartSprite("../GFX/simple.png");
            _spriteHealthBar = new SmartSprite("../GFX/healthbar.png");
        }

        #endregion Private/Protected

        #endregion Methods

    }
}
