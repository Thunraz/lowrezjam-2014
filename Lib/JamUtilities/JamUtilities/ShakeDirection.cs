﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LowRezJam.Utilities
{
    public enum ShakeDirection
    {
        UpDown,
        LeftRight,
        AllDirections
    }
}
