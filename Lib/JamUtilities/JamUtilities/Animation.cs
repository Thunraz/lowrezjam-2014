﻿using System.Collections.Generic;
using SFML.Graphics;
using SFML.Window;

namespace LowRezJam.Utilities
{
    public class Animation
    {
        #region Properties

        /// <summary>
        /// Gets or sets the time for each frame.
        /// </summary>
        /// <value>
        /// The single image time.
        /// </value>
        public float FrameTime { get; set; }

        /// <summary>
        /// The order
        /// </summary>
        public List<int> Order;

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        public Vector2f Position
        {
            get
            {
                return _spriteList[Order[_currentPosition]].Position - (ScreenEffects.ScreenEffects.GlobalSpriteOffset + ScreenEffects.ScreenEffects._dragPosition);
            }
            set
            {
                foreach (var sprite in _spriteList)
                {
                    sprite.Position = value + (ScreenEffects.ScreenEffects.GlobalSpriteOffset + ScreenEffects.ScreenEffects._dragPosition);
                }
            }
        }

        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        /// <value>
        /// The rotation.
        /// </value>
        public float Rotation
        {
            get
            {
                return _spriteList[Order[_currentPosition]].Rotation;
            }
            set
            {
                foreach (var sprite in _spriteList)
                {
                    sprite.Rotation = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the origin.
        /// </summary>
        /// <value>
        /// The origin.
        /// </value>
        public Vector2f Origin
        {
            get
            {
                return _spriteList[Order[_currentPosition]].Origin;
            }
            set
            {
                foreach (var sprite in _spriteList)
                { sprite.Origin = value; }
            }
        }

        /// <summary>
        /// Gets the scale.
        /// </summary>
        /// <value>
        /// The scale.
        /// </value>
        public Vector2f Scale
        {
            get
            {
                return _spriteList[Order[_currentPosition]].Sprite.Scale;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is finished.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is finished; otherwise, <c>false</c>.
        /// </value>
        public bool IsFinished { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance is started.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is started; otherwise, <c>false</c>.
        /// </value>
        public bool IsStarted { get; private set; }

        #endregion Properties

        #region Fields

        private float _currentTime;
        private int _currentPosition;
        private readonly bool _loop;
        private readonly List<SmartSprite> _spriteList;

        #endregion Fields

        #region Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="Animation"/> class.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="spriteSize">Size of the sprite.</param>
        /// <param name="numberOfSprites">The number of sprites.</param>
        /// <param name="singleImageTime">The single image time.</param>
        /// <param name="loop">if set to <c>true</c> [loop].</param>
        public Animation(string fileName, IntRect spriteSize, uint numberOfSprites, float singleImageTime = 0.3f, bool loop = true)
        {
            _loop = loop;
            _spriteList = new List<SmartSprite>();
            Order = new List<int>();
            FrameTime = singleImageTime;
            _currentTime = singleImageTime;
            _currentPosition = 0;
            Texture text = TextureManager.GetTextureFromFileName(fileName);
            for (int i = 0; i < numberOfSprites; i++)
            {
                IntRect rect = new IntRect(i * spriteSize.Width, 0, spriteSize.Width, spriteSize.Height);
                SmartSprite spr = new SmartSprite(text, rect);
                _spriteList.Add(spr);
                Order.Add(i);
            }

        }

        /// <summary>
        /// Updates the animation.
        /// </summary>
        /// <param name="timeObject">The time object.</param>
        public void Update(TimeObject timeObject)
        {
            if (IsStarted)
            {
                _currentTime -= timeObject.ElapsedGameTime;
                if (_currentTime <= 0.0f)
                {
                    _currentTime = FrameTime;

                    if (_loop)
                    {
                        _currentPosition++;
                        if (_currentPosition == Order.Count)
                        {
                            _currentPosition = 0;
                        }
                    }
                    else
                    {
                        _currentPosition++;
                        if (_currentPosition == Order.Count)
                        {
                            _currentPosition--;
                            IsStarted = false;
                            IsFinished = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            _currentPosition = 0;
            IsStarted = true;
            IsFinished = false;
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
            IsStarted = false;
        }

        /// <summary>
        /// Draws the animation to the [RenderWindow].
        /// </summary>
        /// <param name="rw">The rw.</param>
        public void Draw(RenderWindow rw)
        {
            _spriteList[Order[_currentPosition]].Draw(rw);
        }

        #endregion Methods
    }
}
